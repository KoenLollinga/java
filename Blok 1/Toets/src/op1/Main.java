package op1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        float antwoord = 0;

        //maakt nieuwe scanner
         Scanner s = new Scanner(System.in);

         try{
             System.out.println("Welke berekening wilt u? Kiest letter\na. optellen ( + )\nb. aftrekken ( -)" +
                     "\nc. vermeningvuldigen ( * )\nd. delen ( / )");

             String methode = s.nextLine();

             System.out.println("Geef getal 1 op");

             float getal1 = s.nextInt();

             System.out.println("Geeft getal 2 op");

             float getal2 = s.nextInt();

             //kijkt welke getallen en reken metode zijn gekozen en berekent

             if(methode.equals("a") || methode.equals("A")){
                 antwoord = getal1+getal2;
             }
             else if(methode.equals("b") || methode.equals("B")){
                 antwoord = getal1 - getal2;
             }
             else if(methode.equals("c") || methode.equals("C")) {
               antwoord = getal1 * getal2;
             }
             else if(methode.equals("d") || methode.equals("D")){
                antwoord = getal1/getal2;
             }
             // als de gebruiker geen a,b,c of d heeft ingevoerd krijgt een fout melding
             else{
                 System.out.println("U heeft niet a,b,c of d gekozen");

             }
                // kijkt of je door 0 deelt
             if(methode.equals("d") && getal1 ==0 || getal2 == 0){
                 System.out.println("U kan Niet door 0 delen");
             }
             else {

                 System.out.println("Uitkomst van = " + antwoord);
             }
         }
         catch (Exception e){
             System.out.println("Somthing whent wrong");
         }

    }
}
