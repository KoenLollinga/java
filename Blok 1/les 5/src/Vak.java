/**
 * @author R.A. Leito
 * @version 1.0
 * @since Oct 2018
 * MIT License
 * Copyright 2018 R.A. Leito
 */

public class Vak {

    //Properties(eigenschappen)
    private String naam;
    private int duur;
    private String beoordelingsVorm;

    public Vak(String naam, int duur, String beoordelingsVorm) {
        this.naam = naam;
        this.duur = duur;
        this.beoordelingsVorm = beoordelingsVorm;
    }

    public String getNaam(){
        return this.naam;
    }

    /**
     *
     * @return tijd in minuut
     */
    public int getDuur() {
        return duur;
    }

    /**
     *
     * @return tijd in uur
     */
    public float getTijdInUur(){
        return this.duur/60f;
    }


}
