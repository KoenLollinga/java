import java.time.DayOfWeek;
import java.time.LocalDate;


/**
 * @author R.A. Leito
 * @version 1.0
 * @since Oct 2018
 * MIT License
 * Copyright 2018 R.A. Leito
 */

public class TestDag {

        public static void main(String[] args) {
            LocalDate day = LocalDate.now();




            Vak php = new Vak("PHP", 180,"cijfers");
            Vak engels = new Vak("Engels",60,"cijfers");
            Vak sport = new Vak("Sport",60,"Woorden");
            Vak slb = new Vak("SLB",90,"NVT");
            Vak java = new Vak("Java",90,"cijfers");
            Vak froend = new Vak("Frond end",120,"cijfers");
            Vak rekenen = new Vak("rekenen",60,"cijfers");
            Vak db = new Vak("Database",180,"cijfers");
            Vak bur = new Vak("burgerschap",90,"cijfers");
            Vak nederlands = new Vak("Nederlands",90,"Woorden");

            switch(day.getDayOfWeek()){
                case MONDAY:
                    System.out.println("Vandaag heb je eerst " + php.getNaam() + "die duurt " + php.getTijdInUur() +
                            "\ndaarna heb je " + engels.getNaam() + "die duurt " + engels.getTijdInUur() +
                                "\nen als laast heb je " + sport.getNaam() + "die duurt " + sport.getTijdInUur()
                                 + "\n je bent vandaag om 5 uur vrij");
                    break;
                case TUESDAY:
                    System.out.println("Vandaag heb je eerst " + slb.getNaam() + "die duurt " + slb.getTijdInUur() +
                            "\ndaarna heb je " + java.getNaam() + "die duurt " + java.getTijdInUur() +
                            "\nen als laast heb je " + froend.getNaam() + "die duurt " + froend.getTijdInUur()
                            + "\n je bent vandaag om 3 uur vrij");

                    break;

                case WEDNESDAY:
                    System.out.println("Vandaag heb je eerst " + rekenen.getNaam() + "die duurt " + rekenen.getTijdInUur() +
                            "\ndaarna heb je " + php.getNaam() + "die duurt " + php.getTijdInUur() +
                            "\nen als laast heb je " + froend.getNaam() + "die duurt " + froend.getTijdInUur()
                            + "\n je bent vandaag om 3 uur vrij");

                    break;

                case THURSDAY:
                    System.out.println("Vandaag heb je eerst " + db.getNaam() + "die duurt " + db.getTijdInUur() +
                            "\nen als laast heb je " + php.getNaam() + "die duurt " + php.getTijdInUur()
                            + "\n je bent vandaag om 3 uur vrij");

                    break;

                case FRIDAY:
                    System.out.println("Vandaag heb je eerst " + bur.getNaam() + "die duurt " + bur.getTijdInUur() +
                            "\nen als laast heb je " + nederlands.getNaam() + "die duurt " + nederlands.getTijdInUur()
                            + "\n je bent vandaag om 12 uur vrij");


                    break;

                case SATURDAY:
                    System.out.println("Geen school ZUIPEN");

                    break;

                case SUNDAY:
                    System.out.println("Brak dagg");
                    break;
            }


        }
    }

