package opdracht_3_1;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

public class Dobbelsteen {
    private int number;
    private int xAs;
    private int yAs;

    private Rectangle dobRect;
    private Circle dobCircle1,dobCircle2,dobCircle3,dobCircle4,dobCircle5,dobCircle6,dobCircle7;




    public Dobbelsteen(int xAs,int yAs, Pane pane) {

        //dobbelsteen

        // dobsteen frame
        dobRect = new Rectangle(xAs-50,yAs-50,100,100);
        dobRect.setFill(Color.TRANSPARENT);
        dobRect.setStroke(Color.BLACK);
        dobRect.setStrokeWidth(2);

        //1 oog
        dobCircle1 = new Circle(xAs,yAs,10);
        dobCircle1.setFill(Color.TRANSPARENT);
        dobCircle1.setStroke(Color.BLACK);

        //2 oog
        dobCircle2 = new Circle(xAs+25,yAs+25,10);
        dobCircle2.setFill(Color.TRANSPARENT);
        dobCircle2.setStroke(Color.BLACK);
        //3 ogen
        dobCircle3 = new Circle(xAs-25,yAs-25,10);
        dobCircle3.setFill(Color.TRANSPARENT);
        dobCircle3.setStroke(Color.BLACK);

        //4 ogen
        dobCircle4 = new Circle(xAs-25,yAs+25,10);
        dobCircle4.setFill(Color.TRANSPARENT);
        dobCircle4.setStroke(Color.BLACK);
        //5 ogen
        dobCircle5 = new Circle(xAs+25,yAs-25,10);
        dobCircle5.setFill(Color.TRANSPARENT);
        dobCircle5.setStroke(Color.BLACK);
        //6 ogen
        dobCircle6 = new Circle(xAs,yAs-25,10);
        dobCircle6.setFill(Color.TRANSPARENT);
        dobCircle6.setStroke(Color.BLACK);

        //7 ogen
        dobCircle7 = new Circle(xAs,yAs+25,10);
        dobCircle7.setFill(Color.TRANSPARENT);
        dobCircle7.setStroke(Color.BLACK);

        pane.getChildren().add(dobCircle1);



        pane.getChildren().add(dobRect);

    }

    public void trow(int number, Pane pane){
        switch (number){
            case 0:
                pane.getChildren().removeAll(dobCircle1,dobCircle2,dobCircle3,dobCircle4
                        ,dobCircle5,dobCircle6,dobCircle7);

                pane.getChildren().addAll(dobCircle1);


                break;
            case 1:
                pane.getChildren().removeAll(dobCircle1,dobCircle2,dobCircle3,dobCircle4
                        ,dobCircle5,dobCircle6,dobCircle7);

                pane.getChildren().addAll(dobCircle3,dobCircle2);


                break;
            case 2:
                pane.getChildren().removeAll(dobCircle1,dobCircle2,dobCircle3,dobCircle4
                        ,dobCircle5,dobCircle6,dobCircle7);

                pane.getChildren().addAll(dobCircle3,dobCircle2,dobCircle1);

                break;
            case 3:
                pane.getChildren().removeAll(dobCircle1,dobCircle2,dobCircle3,dobCircle4
                        ,dobCircle5,dobCircle6,dobCircle7);

                pane.getChildren().addAll(dobCircle3,dobCircle2,dobCircle4,dobCircle5);

                break;
            case 4:
                pane.getChildren().removeAll(dobCircle1,dobCircle2,dobCircle3,dobCircle4
                        ,dobCircle5,dobCircle6,dobCircle7);

                pane.getChildren().addAll(dobCircle3,dobCircle2,dobCircle4,dobCircle5,dobCircle1);

                break;
            case 5:
                pane.getChildren().removeAll(dobCircle1,dobCircle2,dobCircle3,dobCircle4
                        ,dobCircle5,dobCircle6,dobCircle7);

                pane.getChildren().addAll(dobCircle3,dobCircle2,dobCircle4,dobCircle5,dobCircle6,dobCircle7);

                break;
        }

    }





    public void deleteDobbelsteen(Pane pane){

        pane.getChildren().removeAll(dobCircle1,dobCircle2,dobCircle3,dobCircle4
                ,dobCircle5,dobCircle6,dobCircle7,dobRect);


    }





}


