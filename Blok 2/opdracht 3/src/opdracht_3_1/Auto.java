package opdracht_3_1;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

public class Auto {

    private Circle autoCircle1;
    private Circle autoCircle2;
    private Rectangle autoRect1;
    private Rectangle autoRect2;
    private int xAs=100;
    private int yAs=300;

    public Auto( Pane pane) {



        autoCircle1 = new Circle(xAs+140,yAs+110,15);
        autoCircle2 = new Circle(xAs+10,yAs+110,15);

        autoRect1 = new Rectangle(xAs,yAs,100,100);
        autoRect1.setFill(Color.GREEN);

        autoRect2 = new Rectangle(xAs+100,yAs+50,50,50);
        autoRect2.setFill(Color.GREEN);

        pane.getChildren().addAll(autoCircle1,autoCircle2,autoRect1,autoRect2);

    }

    public void moveRight(int pixels , Pane pane){
                xAs+=pixels;
            autoCircle1.setLayoutX(xAs);
            autoCircle2.setLayoutX(xAs);
            autoRect1.setLayoutX(xAs);
            autoRect2.setLayoutX(xAs);


    }
    public void moveLeft(int pixels, Pane pane){
        xAs-=pixels;
        autoCircle1.setLayoutX(xAs);
        autoCircle2.setLayoutX(xAs);
        autoRect1.setLayoutX(xAs);
        autoRect2.setLayoutX(xAs);

    }

    public void deleteAuto(Pane pane){
        pane.getChildren().removeAll(autoCircle1,autoCircle2,autoRect1,autoRect2);

    }


}
