package opdracht_3_1;

import javafx.scene.layout.Pane;

import javafx.scene.control.Button;

import java.util.Random;

public class Menu {


    private Stoplicht stoplicht;
    private Dobbelsteen dobbelsteen;
    private Auto auto;

    private Button menuStoplichten, knopStoplichtAan, knopStoplichtUit, knopStoplichtLichten;
    private Button menuDobbelstenen, knopDobbelsteenAan, knopDobbelsteenUit, knopDobbelsteenRol;
    private Button menuAuto,knopAutoRechts,knopAutoLinks,knopAutoAan,knopAutoUit;

    private int controlStoplicht = 0;
    private int controlDobbelsteen =0;
    private int controlAuto =0;
    private int getal = 0;


    public Menu(Pane pane) {

        // auto knoppen bouwen

        menuAuto = new Button();
        menuAuto.setLayoutX(50);
        menuAuto.setLayoutY(70);
        menuAuto.setMinWidth(100);
        menuAuto.setText("Auto");

        knopAutoAan = new Button();
        knopAutoAan.setLayoutX(155);
        knopAutoAan.setLayoutY(70);
        knopAutoAan.setMinWidth(100);
        knopAutoAan.setText("Aan");

        knopAutoUit = new Button();
        knopAutoUit.setLayoutX(260);
        knopAutoUit.setLayoutY(70);
        knopAutoUit.setMinWidth(100);
        knopAutoUit.setText("Uit");

        knopAutoLinks = new Button();
        knopAutoLinks.setLayoutX(365);
        knopAutoLinks.setLayoutY(70);
        knopAutoLinks.setMinWidth(100);
        knopAutoLinks.setText("Links");

        knopAutoRechts = new Button();
        knopAutoRechts.setLayoutX(470);
        knopAutoRechts.setLayoutY(70);
        knopAutoRechts.setMinWidth(100);
        knopAutoRechts.setText("Rechts");

        pane.getChildren().addAll(menuAuto);

        menuAuto.setOnAction(ev -> {

            if (controlAuto == 0) {
                pane.getChildren().addAll(knopAutoAan,knopAutoLinks,knopAutoRechts,knopAutoUit);
                controlAuto++;
            } else {
                pane.getChildren().removeAll(knopAutoAan,knopAutoLinks,knopAutoRechts,knopAutoUit);
                controlAuto = 0;
            }

        });

        //auto knoppen acties

        knopAutoAan.setOnAction(ev -> {
            auto = new Auto(pane);
        });

        knopAutoUit.setOnAction(ev -> {
            auto.deleteAuto(pane);
        });

        knopAutoLinks.setOnAction(ev -> {

                auto.moveLeft(20, pane);

        });
        knopAutoRechts.setOnAction(ev -> {

                auto.moveRight(20, pane);

        });



        // stoplichten knoppen bouwen

        menuStoplichten = new Button();
        menuStoplichten.setLayoutX(50);
        menuStoplichten.setLayoutY(10);
        menuStoplichten.setMinWidth(100);
        menuStoplichten.setText("Stoplichten");

        knopStoplichtAan = new Button();
        knopStoplichtAan.setLayoutX(155);
        knopStoplichtAan.setLayoutY(10);
        knopStoplichtAan.setMinWidth(100);
        knopStoplichtAan.setText("Aan");

        knopStoplichtUit = new Button();
        knopStoplichtUit.setLayoutX(260);
        knopStoplichtUit.setLayoutY(10);
        knopStoplichtUit.setMinWidth(100);
        knopStoplichtUit.setText("Uit");

        knopStoplichtLichten = new Button();
        knopStoplichtLichten.setLayoutX(365);
        knopStoplichtLichten.setLayoutY(10);
        knopStoplichtLichten.setMinWidth(100);
        knopStoplichtLichten.setText("Lichten");

        pane.getChildren().add(menuStoplichten);


        menuStoplichten.setOnAction(ev -> {

            if (controlStoplicht == 0) {
                pane.getChildren().addAll(knopStoplichtAan, knopStoplichtUit, knopStoplichtLichten);
                controlStoplicht++;
            } else {
                pane.getChildren().removeAll(knopStoplichtAan, knopStoplichtUit, knopStoplichtLichten);
                controlStoplicht = 0;
            }

        });
        //stoplicht button acties


        if (controlStoplicht == 0) {

            knopStoplichtLichten.setOnAction(ev -> {
                System.out.println("Clicked");
                stoplicht.nextLicht(getal);

                getal++;
                if (getal > 2) {
                    getal = 0;
                }
            });



            knopStoplichtAan.setOnAction(ev -> {
                stoplicht = new Stoplicht(800,100,pane);

            });

            knopStoplichtUit.setOnAction(ev -> {
                stoplicht.deleteStoplicht(pane);
            });


        }
        //einde stoplicht

        //dobbelsteen knoppen bouwen


        menuDobbelstenen = new Button();
        menuDobbelstenen.setLayoutX(50);
        menuDobbelstenen.setLayoutY(40);
        menuDobbelstenen.setMinWidth(100);
        menuDobbelstenen.setText("Dobbelsteen");

        knopDobbelsteenRol = new Button();
        knopDobbelsteenRol.setLayoutX(155);
        knopDobbelsteenRol.setLayoutY(40);
        knopDobbelsteenRol.setMinWidth(100);
        knopDobbelsteenRol.setText("Rol");

        knopDobbelsteenAan = new Button();
        knopDobbelsteenAan.setLayoutX(260);
        knopDobbelsteenAan.setLayoutY(40);
        knopDobbelsteenAan.setMinWidth(100);
        knopDobbelsteenAan.setText("Aan");

        knopDobbelsteenUit = new Button();
        knopDobbelsteenUit.setLayoutX(365);
        knopDobbelsteenUit.setLayoutY(40);
        knopDobbelsteenUit.setMinWidth(100);
        knopDobbelsteenUit.setText("Uit");

        pane.getChildren().add(menuDobbelstenen);


        menuDobbelstenen.setOnAction(ev -> {
            if(controlDobbelsteen ==0){
                pane.getChildren().addAll(knopDobbelsteenAan,knopDobbelsteenRol,knopDobbelsteenUit);
                controlDobbelsteen ++;
            }else{
                pane.getChildren().removeAll(knopDobbelsteenAan,knopDobbelsteenRol,knopDobbelsteenUit);
                controlDobbelsteen =0;
            }
                });


            //dobbelsteen button acties


        knopDobbelsteenUit.setOnAction(ev ->{
            dobbelsteen.deleteDobbelsteen(pane);

        });
        knopDobbelsteenAan.setOnAction(ev ->{
            dobbelsteen= new Dobbelsteen(300,200,pane);
        });
        knopDobbelsteenRol.setOnAction(ev ->{

            Random rand = new Random();
            int number = rand.nextInt((6-0));
            System.out.println(number);
            dobbelsteen.trow(number,pane);

        });




    }
}
