package opdracht_3_1;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;



public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {

        //Bepaal layout om te gebruiken
        Pane layout = new Pane();

        //Nieuwe pane maken met gui elementen
        Gui pane = new Gui(layout);

        //Scene aanmaken met root layout
        Scene scene = new Scene(layout, 1024, 512);

        //Titel voor GUI
        primaryStage.setTitle("Basic GUI");

        //Scene inladen
        primaryStage.setScene(scene);

        //Scene tonen
        primaryStage.show();
    }



}
