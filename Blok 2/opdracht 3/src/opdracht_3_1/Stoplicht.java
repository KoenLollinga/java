package opdracht_3_1;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

public class Stoplicht {
    private Rectangle rectangle;
    private Rectangle pole;
    private Circle circleRed;
    private Circle circleOrange;
    private Circle circleGreen;
    private int xAs;
    private int yAs;

    private int stoplichtIndex;
    private boolean stoplichtBestaat = true;


    public Stoplicht(int xAs, int yAs, Pane pane) {
        this.xAs = xAs;
        this.yAs = yAs;


        //stoplicht
        rectangle = new Rectangle(xAs-38,yAs-30,80,150);
        rectangle.setStrokeWidth(3);
        rectangle.setStroke(Color.BLACK);
        rectangle.setFill(Color.GREY);

        pole = new Rectangle(xAs-10,yAs+120,20,200);

        circleRed = new Circle(xAs,yAs,20, Color.DARKRED);
        circleOrange = new Circle(xAs,yAs+45,20, Color.DARKORANGE);
        circleGreen = new Circle(xAs,yAs+90,20, Color.DARKGREEN);


        pane.getChildren().addAll(rectangle,pole,circleGreen,circleRed,circleOrange);


    }

    public void nextLicht(int stoplichtIndex){
         if(stoplichtIndex > 2 ){
             stoplichtIndex=0;
         }

        if(stoplichtIndex==0){
            circleRed.setFill(Color.RED);
            circleGreen.setFill(Color.DARKGREEN);
        }
        else if (stoplichtIndex==1){
            circleRed.setFill(Color.DARKRED);
            circleOrange.setFill(Color.ORANGE);
        }else{
            circleOrange.setFill(Color.DARKORANGE);
            circleGreen.setFill(Color.GREEN);
        }

    }

    public void deleteStoplicht(Pane pane){
        pane.getChildren().removeAll(rectangle,pole,circleGreen,circleRed,circleOrange);
    }

    public Boolean redlichtStoplict(){

        if (stoplichtIndex == 0 ){
            return true;
        }else return false;
    }


}

