package Test;

public class Pizza {

    private Integer diameter;
    private  String korst;
    private  String saus;


    public Pizza(Integer diameter, String korst, String saus) {
        this.diameter = diameter;
        this.korst = korst;
        this.saus = saus;
    }

    public void setDiameter(int gekozenDiameter){
        this.diameter = gekozenDiameter;

    }
    public void setKorst(String gekozenKorst){
        this.korst = gekozenKorst;

    }
    public void setSaus(String gekozenSaus){
        this.saus = gekozenSaus;

    }
    public String toString(){

    return String.format("diameter: %s\nkorst: %s\nsaus: %s", diameter,korst,saus);
    }
    public String getGeur(){

        String geur = "geur van "+ saus;

        return geur;
    }
    public String isTasty(){
        String tasty;
        if(diameter < 20){
             tasty ="to small to be tasty";
        }
        else {
            tasty="it sure look apatizing";
        }
        return tasty;
    }

}
