package opdracht_2_3;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.Collections;


public class Gui {

    private final Button knop;
    private final TextField tekkstvakNaam;
    private final TextField tekstvakLengte;
    private final TextField tekstvakGewicht;
    private final Text text;

    /**
     * Constructor
     *
     * @param p Same layout type as main application
     */
    public Gui(VBox p) {

        //Init alle properties
        knop = new Button();
        tekkstvakNaam = new TextField();
        tekstvakGewicht = new TextField();
        tekstvakLengte = new TextField();
        text = new Text();

        knop.setText("Test");
        knop.setOnAction(ev -> {
            handle();
        });


        p.getChildren().add(knop);
        p.getChildren().add(tekkstvakNaam);
        p.getChildren().add(tekstvakGewicht);
        p.getChildren().add(tekstvakLengte);
        p.getChildren().add(text);
    }


    public void handle() {
        System.out.println("Clicked");
        String tekstNaam = tekkstvakNaam.getText();
        String tekstGewicht = tekstvakGewicht.getText();
        String tekstLengte = tekstvakLengte.getText();

        double doubleGewicht = Integer.parseInt(tekstGewicht);
        double doubleLengte = Integer.parseInt(tekstLengte);

        doubleLengte /=100;

        double bmi = doubleGewicht/ (doubleLengte*2);

        String output = "naam : "+tekstNaam+ "\n" +
        "gewicht : " +doubleGewicht+ "\n" +
        "lengte : " +doubleLengte+"\n"+
        "BMI : " + bmi ;

        text.setText(String.format(output));


    }


}
