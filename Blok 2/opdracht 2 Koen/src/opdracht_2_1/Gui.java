//package opdracht_2_1;
//
//import javafx.scene.control.Button;
//import javafx.scene.control.TextField;
//import javafx.scene.layout.VBox;
//import javafx.scene.text.Text;
//
//
//public class Gui {
//
//    private final Button knop;
//    private final TextField tekstvak;
//    private final Text text;
//
//    /**
//     * Constructor
//     * @param p Same layout type as main application
//     */
//    public Gui(VBox p) {
//
//        //Init alle properties
//        knop = new Button();
//        tekstvak = new TextField();
//        text = new Text();
//
//        knop.setText("Test");
//        knop.setOnAction(ev ->{
//            handle();
//        });
//
//
//        p.getChildren().add(knop);
//        p.getChildren().add(tekstvak);
//        p.getChildren().add(text);
//    }
//
//
//    public void handle(){
//        System.out.println("Clicked");
//        String t = tekstvak.getText();
//        int tafel = Integer.parseInt(t);
//        String uitprinten = "";
//        for (int i = 1; i < 11; i++){
//            int anwser = i*tafel;
//
//             uitprinten += tafel + " * " + i + " = "+ anwser + "\n";
//        }
//
//        text.setText(String.format("De uitkomsten zijn : \n%s",uitprinten));
//    }
//
//
//
//}
