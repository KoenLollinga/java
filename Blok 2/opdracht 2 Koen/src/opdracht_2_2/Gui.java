//package opdracht_2_2;
//
//import javafx.scene.control.Button;
//import javafx.scene.control.TextField;
//import javafx.scene.layout.VBox;
//import javafx.scene.text.Text;
//
//import java.util.ArrayList;
//import java.util.Collections;
//
//
//public class Gui {
//
//    private final Button knop;
//    private final TextField tekstvakGetal1;
//    private final TextField tekstvakGetal3;
//    private final TextField tekstvakGetal2;
//    private final Text text;
//
//    /**
//     * Constructor
//     *
//     * @param p Same layout type as main application
//     */
//    public Gui(VBox p) {
//
//        //Init alle properties
//        knop = new Button();
//        tekstvakGetal1 = new TextField();
//        tekstvakGetal2 = new TextField();
//        tekstvakGetal3 = new TextField();
//        text = new Text();
//
//        knop.setText("Test");
//        knop.setOnAction(ev -> {
//            handle();
//        });
//
//
//        p.getChildren().add(knop);
//        p.getChildren().add(tekstvakGetal1);
//        p.getChildren().add(tekstvakGetal2);
//        p.getChildren().add(tekstvakGetal3);
//        p.getChildren().add(text);
//    }
//
//
//    public void handle() {
//        System.out.println("Clicked");
//        String getal1Text = tekstvakGetal1.getText();
//        String getal2Text = tekstvakGetal2.getText();
//        String getal3Text = tekstvakGetal3.getText();
//
//        int getal1 = Integer.parseInt(getal1Text);
//        int getal2 = Integer.parseInt(getal2Text);
//        int getal3 = Integer.parseInt(getal3Text);
//
//        ArrayList<Integer> getallen = new ArrayList<Integer>();
//
//        getallen.add(0, getal1);
//        getallen.add(1, getal2);
//        getallen.add(2, getal3);
//
//        Collections.sort(getallen);
//        String stringGetallen = "";
//        for (int i = 0; i < getallen.size(); i++) {
//
//            stringGetallen += "getal" + (i + 1) + " = " + getallen.get(i) + "\n";
//
//
//        }
//        text.setText(String.format(stringGetallen));
//
//
//    }
//
//
//}
