package op2;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.Collections;


public class GuiLayout {

    private final Button addKnop, berkenKnop, resetKnop;
    private final TextField tekstvak;
    private final Text getallenText, berekenText;

    private ArrayList<Double> getallen;


    public GuiLayout(VBox p) {

        //Init alle properties
        addKnop = new Button();
        berkenKnop = new Button();
        resetKnop = new Button();
        tekstvak = new TextField();
        getallenText = new Text();
        berekenText = new Text();

        // bereken knop
        berkenKnop.setText("Calculate");
        berkenKnop.setOnAction(ev -> {
            bereken();
        });
        // toevoeg knop
        addKnop.setText("ADD");
        addKnop.setOnAction(ev -> {
            handle();
        });
        //resetknop
        resetKnop.setText("Reset");
        resetKnop.setOnAction(ev -> {
            reset();
        });
        tekstvak.setMaxWidth(150);


        p.getChildren().add(addKnop);
        p.getChildren().add(tekstvak);
        p.getChildren().add(resetKnop);
        p.getChildren().add(berkenKnop);
        p.getChildren().add(getallenText);
        p.getChildren().add(berekenText);

        ArrayList getallen = new ArrayList<>();

    }


    int clicked = 0;
    private int index =0;

    // handelt add knop
    public void handle() {
        System.out.println("Clicked");

        clicked++;

        String getalText = tekstvak.getText();

        Double getal = Double.parseDouble(getalText);

        //gaat iets fout mee idk wat
        getallen.add(index,getal);


        for (int i = 0; i < getallen.size(); i++) {

            getallenText.setText("Getallen zijn :"+ getallen.get(i));

        }

        index++;

    }
    // handelte berken knop
    public void bereken() {

        double maxGetal = Collections.max(getallen);
        double minGetal = Collections.min(getallen);
        double total =0;
        double midGetal =0;
        double aantaalGetallen = getallen.size();

        for (int i = 0; i < getallen.size(); i++) {

              total =+ getallen.get(i);
            midGetal = total / getallen.size();
        }


        berekenText.setText("Max getal is " + maxGetal + "\nMin getal is " + minGetal +
                "\ngemiddelde is " + midGetal + "\n getallen is " + aantaalGetallen);


    }
    //handelreset knop
    public void reset(){
        getallen.clear();
        index =0;


    }


}

