package op2;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


/**
 * @author R.A. Leito
 * @version 1.0
 * @since Nov 2018
 * MIT License
 * Copyright 2018 R.A. Leito
 */

public class BasicGui extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {

        //Bepaal layout om te gebruiken
        VBox root = new VBox();

        //Nieuwe pane maken met gui elementen
        GuiLayout pane = new GuiLayout(root);

        //Scene aanmaken met root layout
        Scene scene = new Scene(root, 1024, 512);

        //Titel voor GUI
        primaryStage.setTitle("Basic GUI");

        //Scene inladen
        primaryStage.setScene(scene);

        //Scene tonen
        primaryStage.show();
    }



}