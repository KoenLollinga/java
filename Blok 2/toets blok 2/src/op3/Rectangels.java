package op3;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.control.TextField;


import java.util.ArrayList;

public class Rectangels {

    private ArrayList<Rectangle> rect;
    private Pane pane;
    private int hoeveelheidRect;
    private TextField text;


    public Rectangels(Pane pane, int hoeveelheidRect) {


        rect = new ArrayList<Rectangle>();


        int rectSize = 20;
        int as = 0;
        // hier word naar de hoeveelheid aangeven rectangles gemaakt
        for (int i = 0; i < hoeveelheidRect; i++) {


            rectSize += 20;

            as = rectSize / 2;


            //rect build
            rect.add(new Rectangle(230 - as, 230 - as, rectSize, rectSize));


            rect.get(i).setFill(Color.TRANSPARENT);
            rect.get(i).setStroke(Color.BLACK);
            rect.get(i).setStrokeWidth(1);

            pane.getChildren().add(rect.get(i));

        }


    }

    // hier word alle rectangles verwijdert
    public void resetRect(Pane pane, int hoeveelheidRect) {
        System.out.println("test");
        for (int i = 0; i < hoeveelheidRect; i++) {
            pane.getChildren().remove(rect.get(i));
            System.out.println();

        }


    }


}
