package op3;

import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import javafx.scene.control.Button;

import java.util.ArrayList;

public class Gui {

    private Rectangle rect;
    private Button go, reset;
    private TextField text;
    private Rectangels rectangels;

    private int buttonToggle = 0;


    public Gui(Pane l) {

// go button
        go = new Button("go");
        go.setLayoutX(5);
        go.setLayoutY(10);

        go.setOnAction(ev -> {

            String t = text.getText();
           int hoeveelheidRect = Integer.parseInt(t);

         rectangels = new Rectangels(l,hoeveelheidRect);
        });

        l.getChildren().add(go);

        //text field
        text = new TextField();

        l.getChildren().add(text);
        text.setLayoutX(60);
        text.setLayoutY(10);

        //reset button
        reset = new Button("reset");

        reset.setLayoutX(5);
        reset.setLayoutY(40);

        l.getChildren().add(reset);

        reset.setOnAction(ev -> {

            String t = text.getText();
            int hoeveelheidRect = Integer.parseInt(t);
            rectangels.resetRect(l,hoeveelheidRect);
        });


    }



}


