package op1;

import java.util.ArrayList;

/**
 * d2v3
 *
 * @author R.A. Leito
 * @version 1.0
 * @since Jan 2019
 * MIT License
 * Copyright 2019 R.A. Leito
 */

public class ArtikelManager {

    private final ArrayList<Artikel> voorraad;


    public ArtikelManager() {
        voorraad = new ArrayList<Artikel>();
    }


    public void addArtikel(Artikel artikel){
        voorraad.add(artikel);
    }

    public String toonVoorraad(){

        String output = "Voorraad\n______________________\n\n";

        for (Artikel artikel : voorraad) {
            output += artikel.toString();

        }

        return output;

    }




}
