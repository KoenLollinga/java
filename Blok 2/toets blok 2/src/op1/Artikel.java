package op1;

public class Artikel {

    String merk;
    String omschrijving;
    double inkoopPrijs;
    double marge;
    double verkoopPrijs;

    //eerste constructor met marge

    public Artikel(String merk, String omschrijving, double inkoopPrijs, double marge) {
        this.merk = merk;
        this.omschrijving = omschrijving;
        this.inkoopPrijs = inkoopPrijs;
        this.marge = marge;

    }
    //constructor zonder marge
    public Artikel(String merk, String omschrijving, double inkoopPrijs) {
        this.merk = merk;
        this.omschrijving = omschrijving;
        this.inkoopPrijs = inkoopPrijs;

    }

    // marge setter
    public void setMarge(double marge) {

        this.marge = marge;



    }


// verkooprijs getter
    public String toString() {
        double verkoopPrijs = inkoopPrijs * (marge +1);


        String output = "" + merk + "\n Inkoopprijs         " + inkoopPrijs + "\nVerkoopprijs        " + verkoopPrijs + "\n";


        return output;
    }


}
