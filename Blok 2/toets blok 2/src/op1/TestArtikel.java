package op1;

/**
 * d2v3
 *
 * @author R.A. Leito
 * @version 1.0
 * @since Jan 2019
 * MIT License
 * Copyright 2019 R.A. Leito
 */

public class TestArtikel {
    public static void main(String[] args) {


        ArtikelManager vooraad1 = new ArtikelManager();

        Artikel tandenborstel1 = new Artikel("Oral-B","Pro 2", 40.00, 0.2 );
        Artikel tandenborstel2 = new Artikel("Oral-B","Pro 2000", 89.50 );
        Artikel tandenborstel3 = new Artikel("Oral-B","Pro 2300", 93.20);
        Artikel tandenborstel4 = new Artikel("Oral-B","Pro 3000", 119.99, 0.3 );

        vooraad1.addArtikel(tandenborstel1);
        vooraad1.addArtikel(tandenborstel2);
        vooraad1.addArtikel(tandenborstel3);
        vooraad1.addArtikel(tandenborstel4);


        System.out.println(vooraad1.toonVoorraad());

        tandenborstel1.setMarge(.25);

        System.out.println(vooraad1.toonVoorraad());

    }
}
