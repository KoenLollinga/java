import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;



public class GuiLayout {

    private final Button knop;
    private final TextField tekstvak;
    private final Text text;

    /**
     * Constructor
     * @param p Same layout type as main application
     */
    public GuiLayout(VBox p) {

        //Init alle properties
        knop = new Button();
        tekstvak = new TextField();
        text = new Text();

        knop.setText("Test");
        knop.setOnAction(ev ->{
            handle();
        });


        p.getChildren().add(knop);
        p.getChildren().add(tekstvak);
        p.getChildren().add(text);
    }


    public void handle(){
        System.out.println("Clicked");
        String t = tekstvak.getText();
        text.setText(String.format("Een druk op de knop en ingevuld: %s", t));

    }



}
