public class Student {

    private String naam;
    private String klas;
    private final String opleiding;
    private int studentenNummer;

    public Student(String naam, String klas, int studentenNummer,String opleiding) {
        this.naam = naam;
        this.klas = klas;
        this.studentenNummer = studentenNummer;
        this.opleiding = opleiding;


    }

    public void setKlas(String klas){
        this.klas = klas;
    }


    public String getKlas() {
        return klas;
    }

    public String getOpleiding() {
        return opleiding;
    }

    public String getNaam() {
        return naam;
    }

    public int getStudentenNummer() {
        return studentenNummer;
    }


    public String toString(){
        return String.format("Naam %s,\nklas %s,\nOpleiding %s\nStudentennummer %s",naam,klas,opleiding,studentenNummer);
    }







}
