/**
 * UitlegGui
 *
 * @author R.A. Leito
 * @version 1.0
 * @since Dec 2018
 * MIT License
 * Copyright 2018 R.A. Leito
 */


import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class App extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage window) {

        VBox layout = new VBox(20);

        LayoutGui layoutGui = new LayoutGui(layout);


        Scene scene = new Scene(layout, 800, 600);
        scene.getStylesheets().add("style.css");


        window.setTitle("Uitleg GUI");

        window.setScene(scene);

        window.show();

    }
}
