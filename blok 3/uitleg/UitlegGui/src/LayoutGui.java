import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

/**
 * UitlegGui
 *
 * @author R.A. Leito
 * @version 1.0
 * @since Dec 2018
 * MIT License
 * Copyright 2018 R.A. Leito
 */

public class LayoutGui {

    private final TextField invulveld;
    private final Button knop1;
    private final Text txt;


    public LayoutGui(VBox l) {

        HBox topBox = new HBox(10);


        knop1 = new Button();
        knop1.setText("Drukken maar");

        l.setPadding(new Insets(10));

        invulveld = new TextField();

        txt = new Text();


        topBox.getStyleClass().add("menu");

        knop1.setOnAction(ev -> {
            handle();
        });

        l.getChildren().addAll(topBox, txt);
        topBox.getChildren().addAll(invulveld, knop1);

    }


    public void handle(){

        String waarde = invulveld.getText();
        System.out.println(waarde);
        txt.setText(waarde);
        invulveld.clear();


    }


}
