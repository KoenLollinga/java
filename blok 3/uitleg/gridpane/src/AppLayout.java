import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;


/**
 * BorderPane
 *
 * @author R.A. Leito
 * @version 1.0
 * @since Mar 2019
 * MIT License
 * Copyright 2019 R.A. Leito
 */

public class AppLayout {

    private VBox layoutApp;
    private final GridPane gp;
    private final TextField invoer;

    public AppLayout(VBox layout) {

        layoutApp = layout;

        layoutApp.setPadding(new Insets(10));

        invoer = new TextField();
        invoer.setPromptText("0");
        invoer.setAlignment(Pos.BASELINE_RIGHT);
        gp = new GridPane();

        layoutApp.getChildren().addAll(invoer, gp);

        Button b1 = new Button("1");
        Button b2 = new Button("2");
        Button b3 = new Button("3");
        Button b4 = new Button("4");
        Button b5 = new Button("5");
        Button b6 = new Button("6");
        Button b7 = new Button("7");
        Button b8 = new Button("8");
        Button b9 = new Button("9");
        Button b0 = new Button("0");

        Pane p = new Pane();
        p.setPadding(new Insets(5));
        p.getStyleClass().add("yellow-bg");

        gp.add(b7,0,0);
        gp.add(b8,1,0);
        gp.add(b9,2,0);
        gp.add(b4,0,1);
        gp.add(b5,1,1);
        gp.add(b6,2,1);
        gp.add(b1,0,2);
        gp.add(b2,1,2);
        gp.add(b3,2,2);
        gp.add(new Button("."),0,3);
        gp.add(b0,1,3);
        gp.add(new Button("del"),2,3);

        gp.add(p, 4, 0, 1,4);










    }


    public void handle(){




    }
}
