/**
 * newGui
 *
 * @author R.A. Leito
 * @version 1.0
 * @since Mar 2019
 * MIT License
 * Copyright 2019 R.A. Leito
 */


import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class App extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage window) {

        BorderPane layout = new BorderPane();


        Scene scene = new Scene(layout, 800, 600);


        AppLayout appLayout = new AppLayout(layout);

        scene.getStylesheets().add("style.css");

        window.setTitle("Uitleg GUI");

        window.setScene(scene);

        window.show();


    }
}
