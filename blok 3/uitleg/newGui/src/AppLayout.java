import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * newGui
 *
 * @author R.A. Leito
 * @version 1.0
 * @since Mar 2019
 * MIT License
 * Copyright 2019 R.A. Leito
 */

public class AppLayout {

    private final TextField invulveld;
    private final Button knop1;
    private final Text txt;


    public AppLayout(HBox l) {


        knop1 = new Button();
        knop1.setText("Drukken maar");

        l.setPadding(new Insets(10));

        invulveld = new TextField();

        txt = new Text();
11

        knop1.setOnAction(ev -> {
            handle();
        });


        l.getChildren().addAll(invulveld, knop1, txt);

    }


    public void handle(){

        String waarde = invulveld.getText();
        System.out.println(waarde);
        txt.setText(waarde);
        invulveld.clear();


    }
}
