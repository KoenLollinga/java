package op2;

import javax.imageio.ImageIO;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class ImageRender {

    private String lib;
    private BufferedImage img = null;

    public ImageRender(String lib) {
        this.lib = lib;

    }

    public BufferedImage render(String fileName) {

        System.out.println(lib+fileName);
        try {
            img = ImageIO.read(new File(lib + fileName));

        } catch (IOException e) {
            System.out.println(e);

        }
        return img;
    }

    public ArrayList<BufferedImage> giveSprites(BufferedImage spriteSheet, int width, int height ,int spriteHeight,int spriteWidth) {
        ArrayList<BufferedImage> list = new ArrayList<BufferedImage>();
        int x = 0;
        int y = 0;
        int w = spriteWidth;
        int h = spriteHeight;


        for (int i = 0; i < height; i += h) {
            System.out.println(i);
            for (int i1 = 0; i1 < width; i1 += w) {
                BufferedImage img = spriteSheet.getSubimage(x, y, w, h);
                System.out.println(i1 + "i1");
                list.add(img);
             x+=w;
            }
            x=0;
            y+=h;
        }


        return list;
    }

    public void setLib(String lib) {
        this.lib = lib;
    }

//    public BufferedImage animateSprite(int spriteFrames , int tick){
//
//      return
//    }


}




