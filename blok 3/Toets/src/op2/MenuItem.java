package op2;


public class MenuItem {
   public String omschrijving;
   public Double prijs;
   private int items;
   private int id;

    public MenuItem(String korteOmschrijving, double prijs, int id) {
        this.omschrijving = korteOmschrijving;
        this.prijs = prijs;
        this.id = id;
        items=0;
    }

    public int getId() {
        return id;
    }

    public void addItem(){
        if (items < 49){
            items++;
        }
    }
    public void resetItem(){
        items=0;
    }
    public int getMenuItem(){
        return items;
    }

    public void setKorteOmschrijving(String omschrijving) {
        this.omschrijving = omschrijving;
    }

    public void setPrijs(double prijs) {
        this.prijs = prijs;
    }

    public String getKorteOmschrijving() {
        return omschrijving;
    }

    public Double getPrijs() {
        return prijs;
    }

    public String showMenuItem() {
        String output = "item :" + getKorteOmschrijving() +
                "\nprijs :" + getPrijs() ;
        return output;
    }
}
