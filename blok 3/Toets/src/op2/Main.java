package op2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferStrategy;

public class Main {

    public static void main(String[] args) {
        int width = 300, height = 600;

        //new frame

        JFrame frame = new JFrame();

        frame.setSize(width, height);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.setVisible(true);

        // new canvas

        Canvas canvas = new Canvas();

        canvas.setSize(width, height);
        canvas.setBackground(Color.white);
        canvas.setVisible(true);
        canvas.setFocusable(false);


        frame.add(canvas);

        canvas.createBufferStrategy(3);

        boolean running = true;

        Graphics g;


        BufferStrategy bufferStrategy;
        bufferStrategy = canvas.getBufferStrategy();
        g = bufferStrategy.getDrawGraphics();
        g.clearRect(0, 0, width, height);
        g.setFont(new Font("TimesRoman", Font.BOLD, 24));

        Draw draw = new Draw(g);
        ButtonManager buttonManager = new ButtonManager(draw);

        //maak mouse listener en voeg toe aan canvas
        MouseListener mListener = new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {
                int mouseY = e.getY();
                int mouseX = e.getX();
                buttonManager.setMouseX(mouseX);
                buttonManager.setMouseY(mouseY);


            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        };
        canvas.addMouseListener(mListener);




        int screen =0;
        int checkScreen=0;
        buttonManager.setActiveScreen(screen);



        while (running) {
            bufferStrategy = canvas.getBufferStrategy();
            g = bufferStrategy.getDrawGraphics();
            g.clearRect(0, 0, width, height);



            //check of screen verandert is
            if (checkScreen != screen){
                screen = checkScreen;
                buttonManager.setAllButtonsUnactive();
                buttonManager.setActiveScreen(screen);
            }
               draw.setWinkelmandje(buttonManager.winkelmandje());


            draw.render(screen);

            // op scene change knop gedrukt krijgt checkScreen nieuwe waarde
            checkScreen = buttonManager.handleClicked(screen);



            bufferStrategy.show();
            g.dispose();

        }
    }



}

