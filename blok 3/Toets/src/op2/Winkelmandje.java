package op2;


import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Random;

public class Winkelmandje {


    private ArrayList<MenuItem> allMenuItems;
    private double totalPrice = 0;
    private DecimalFormat f;
    private int orderNummer;


    public Winkelmandje() {
        allMenuItems = new ArrayList<>();
        allMenuItems.add(0, new MenuItem("test", 0, 0));
        f = new DecimalFormat("#.##");
        orderNummer =1;
        //burgers
        allMenuItems.add(1, new MenuItem("Vega burger", 3.25, 1));
        allMenuItems.add(2, new MenuItem("Vis burger", 4.15, 2));
        allMenuItems.add(3, new MenuItem("Vlees burger", 3.85, 3));

        //dranken
        allMenuItems.add(4, new MenuItem("Bier", 2.90, 4));
        allMenuItems.add(5, new MenuItem("Cola", 2.25, 5));
        allMenuItems.add(6, new MenuItem("7-UP", 2.25, 6));

        //friet
        allMenuItems.add(7, new MenuItem("Ras friet", 1.85, 7));
        allMenuItems.add(8, new MenuItem("Krul friet", 1.95, 8));
        allMenuItems.add(9, new MenuItem("Friet", 1.75, 9));

    }

    public void addItem(int id) {

        allMenuItems.get(id).addItem();
        totalPrice += allMenuItems.get(id).prijs;

    }

    public Integer getItem(int id) {
        return allMenuItems.get(id).getMenuItem();

    }

    public String getTotalPrice() {
        return f.format(totalPrice);
    }

    public void emptyWinkelmandje() {
        for (MenuItem allMenuItem : allMenuItems) {
            allMenuItem.resetItem();
        }
        orderNummer++;
        totalPrice=0;

    }

    public int getOrderNummer() {
        return orderNummer;
    }

    public ArrayList<String> getWinkelmandje() {
        ArrayList<String> output = new ArrayList<>();
        String item;
        Double totalPrice;

        for (MenuItem allMenuItem : allMenuItems) {
            if (allMenuItem.getMenuItem() > 0) {
                totalPrice = allMenuItem.getMenuItem() * allMenuItem.prijs;
                item = allMenuItem.showMenuItem() + " X " + allMenuItem.getMenuItem() + " = " + f.format(totalPrice);
                output.add(item);
            }
        }

        return output;

    }


}
