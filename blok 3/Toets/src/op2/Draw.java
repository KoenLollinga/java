package op2;


import java.awt.*;
import java.awt.image.BufferedImage;
import java.nio.file.Paths;
import java.util.ArrayList;


public class Draw {
    private Integer screen;
    //FIXME hardcoded URL
//    private String lib = "C:\\Users\\User\\OneDrive\\School 2018\\java\\blok 3\\Toets\\res\\";
    private String path = Paths.get("").toAbsolutePath().toString();
    private String lib = path + "/res/";

    private ImageRender imageRender = new ImageRender(lib);
    private BufferedImage startScreen, bottomButPressed, backBut, burgerLink, frietLink, drankenLink, menuButtOff, menuButtOn, winkelmandjeBut;
    private Graphics graphics;
    private Winkelmandje winkelmandje;
    private ArrayList<String> $winkelmandje;
    //button locations
    private int bottomButPressedX = 0, bottomButPressedY = 450;
    private int backButX = 0, backButY = 0;
    private int drankenLinkX = 50, drankenLinkY = 100;
    private int frietLinkX = 50, frietLinkY = 225;
    private int burgerLinkX = 50, buregerLinkY = 350;
    private int buttTopX = 200, buttTopY = 65;
    private int buttMidX = 200, buttMidY = 190;
    private int buttBottX = 200, buttBottY = 315;
    private int winkelmandButX = 220, winkelmandButY = 5;


    public Draw(Graphics graphics) {
        System.out.println(lib);
        this.graphics = graphics;
        startScreen = imageRender.render("startScreen.png");
        backBut = imageRender.render("backBut.png");
        bottomButPressed = imageRender.render("bottomButPressed.png");
        drankenLink = imageRender.render("drankenLink.png");
        frietLink = imageRender.render("frietLink.png");
        burgerLink = imageRender.render("burgerLink.png");
        menuButtOff = imageRender.render("menuButtOff.png");
        menuButtOn = imageRender.render("menuButtOn.png");
        winkelmandjeBut = imageRender.render("winkelmandje.png");


    }

    public void render(Integer screen) {
        switch (screen) {
            case 0:
                graphics.drawImage(startScreen, 0, 0, null);
                graphics.drawImage(bottomButPressed, bottomButPressedX, bottomButPressedY, null);

                graphics.drawString("NEXT", 110, 520);
                break;
            case 1:

                //hoofdmenu
                graphics.drawImage(backBut, backButX, backButY, null);
                graphics.drawImage(frietLink, frietLinkX, frietLinkY, null);
                graphics.drawImage(drankenLink, drankenLinkX, drankenLinkY, null);
                graphics.drawImage(burgerLink, burgerLinkX, buregerLinkY, null);
                graphics.drawImage(winkelmandjeBut, winkelmandButX, winkelmandButY, null);


                break;
            case 2:
                //burger keuze menu
                graphics.drawImage(backBut, backButX, backButY, null);
                graphics.drawImage(menuButtOff, buttTopX, buttTopY, null);

                graphics.drawString("VEGA", 80, 100);
                graphics.drawImage(menuButtOff, buttMidX, buttMidY, null);
                drawWinkelmandje(1, 0);


                graphics.drawString("VIS", 80, 225);
                graphics.drawImage(menuButtOff, buttBottX, buttBottY, null);
                drawWinkelmandje(2, 1);

                graphics.drawString("VLEES", 80, 350);
                graphics.drawImage(bottomButPressed, bottomButPressedX, bottomButPressedY, null);
                drawWinkelmandje(3, 2);

                graphics.drawString("Akoord", 100, 520);


                break;

            case 3:
                //dranken menu

                graphics.drawImage(backBut, backButX, backButY, null);
                //vega
                graphics.drawImage(menuButtOff, buttTopX, buttTopY, null);

                //vis
                graphics.drawString("Bier", 80, 100);
                graphics.drawImage(menuButtOff, buttMidX, buttMidY, null);
                drawWinkelmandje(4, 0);
                graphics.drawString("COLA", 80, 225);
                //vlees
                graphics.drawImage(menuButtOff, buttBottX, buttBottY, null);
                drawWinkelmandje(5, 1);

                graphics.drawString("7-UP", 80, 350);
                graphics.drawImage(bottomButPressed, bottomButPressedX, bottomButPressedY, null);
                drawWinkelmandje(6, 2);
                graphics.drawString("Akoord", 100, 520);

                break;

            case 4:
                //frietMenu
                graphics.drawImage(backBut, backButX, backButY, null);

                graphics.drawImage(menuButtOff, buttTopX, buttTopY, null);


                graphics.drawString("Ras", 80, 100);
                graphics.drawImage(menuButtOff, buttMidX, buttMidY, null);
                drawWinkelmandje(7, 0);

                graphics.drawString("Krul", 80, 225);
                graphics.drawImage(menuButtOff, buttBottX, buttBottY, null);
                drawWinkelmandje(8, 1);

                graphics.drawString("Normaal", 80, 350);
                graphics.drawImage(bottomButPressed, bottomButPressedX, bottomButPressedY, null);
                drawWinkelmandje(9, 2);
                graphics.drawString("Akoord", 100, 520);

                break;

            case 5:
                //winkelMandje
                graphics.drawImage(backBut, backButX, backButY, null);
                graphics.drawImage(bottomButPressed, bottomButPressedX, bottomButPressedY, null);
                graphics.drawString("Checkout", 95, 520);
                if (winkelmandje.getWinkelmandje().size() > 0) {
                    int y = 80;
                    graphics.setFont(new Font("TimesRoman", Font.BOLD, 12));

                    for (String item : winkelmandje.getWinkelmandje()) {
                        graphics.drawString(item, 30, y);
                        y += 30;

                    }

                    String totalPrice = winkelmandje.getTotalPrice();
                    int orderNummer = winkelmandje.getOrderNummer();

                    graphics.drawString("Totaal : € " + totalPrice, 50, y + 30);
                    graphics.setFont(new Font("TimesRoman", Font.BOLD, 24));
                    graphics.drawString("NR" +orderNummer,35,35);

                }

                break;
        }
    }

    public void setWinkelmandje(Winkelmandje winkelmandje) {
        this.winkelmandje = winkelmandje;
    }

    //int button 0 top 1 mid 2 bottom
    private void drawWinkelmandje(int id, int button) {
        if (winkelmandje.getItem(id) > 0) {
            int y = 40;
            int x = 15;

            String str = winkelmandje.getItem(id).toString();

            switch (button) {
                case 0:
                    graphics.drawString("+" + str, buttTopX + x, buttTopY + y);
                    break;
                case 1:
                    graphics.drawString("+" + str, buttMidX + x, buttMidY + y);
                    break;

                case 2:
                    graphics.drawString("+" + str, buttBottX + x, buttBottY + y);
                    break;

            }

        }

    }
//button locatie getters

    public int getBottomButPressedX() {
        return bottomButPressedX;
    }

    public int getBottomButPressedY() {
        return bottomButPressedY;
    }

    public int getBackButX() {
        return backButX;
    }

    public int getBackButY() {
        return backButY;
    }

    public int getDrankenLinkX() {
        return drankenLinkX;
    }

    public int getDrankenLinkY() {
        return drankenLinkY;
    }

    public int getFrietLinkX() {
        return frietLinkX;
    }

    public int getFrietLinkY() {
        return frietLinkY;
    }

    public int getBurgerLinkX() {
        return burgerLinkX;
    }

    public int getBuregerLinkY() {
        return buregerLinkY;
    }

    public int getButtTopX() {
        return buttTopX;
    }

    public int getButtTopY() {
        return buttTopY;
    }

    public int getButtMidX() {
        return buttMidX;
    }

    public int getButtMidY() {
        return buttMidY;
    }

    public int getButtBottX() {
        return buttBottX;
    }

    public int getButtBottY() {
        return buttBottY;
    }

    public Integer getScreen() {
        return screen;
    }

    public int getWinkelmandButX() {
        return winkelmandButX;
    }

    public int getWinkelmandButY() {
        return winkelmandButY;
    }
}
