package op2;


import java.util.ArrayList;

public class ButtonManager {
    private InvisButton bottomButt, backButt, burgerLink, drankenLink, frietLink, menuButTop, menuButMid, menuButBottom, winkelmandjeButt;
    private ArrayList<InvisButton> allButtons, activeButtons;
    private int mouseX, mouseY;
    private Winkelmandje winkelmandje;

    public ButtonManager(Draw screen) {
        // maak alle buttons over de foto's

        allButtons = new ArrayList<InvisButton>();
        winkelmandje = new Winkelmandje();


        bottomButt = new InvisButton(screen.getBottomButPressedX(), screen.getBottomButPressedY());
        bottomButt.setHeight(150);
        bottomButt.setWidth(300);
        bottomButt.setUnactive();
        allButtons.add(bottomButt);

        backButt = new InvisButton(screen.getBackButX(), screen.getBackButY());
        backButt.setHeight(20);
        backButt.setWidth(20);
        backButt.setUnactive();
        allButtons.add(backButt);

        burgerLink = new InvisButton(screen.getBurgerLinkX(), screen.getBuregerLinkY());
        burgerLink.setHeight(100);
        burgerLink.setWidth(200);
        burgerLink.setUnactive();
        allButtons.add(burgerLink);


        drankenLink = new InvisButton(screen.getDrankenLinkX(), screen.getDrankenLinkY());
        drankenLink.setHeight(100);
        drankenLink.setWidth(200);
        drankenLink.setUnactive();
        allButtons.add(drankenLink);


        frietLink = new InvisButton(screen.getFrietLinkX(), screen.getFrietLinkY());
        frietLink.setHeight(100);
        frietLink.setWidth(200);
        frietLink.setUnactive();
        allButtons.add(frietLink);

        menuButBottom = new InvisButton(screen.getButtBottX(), screen.getButtBottY());
        menuButBottom.setHeight(60);
        menuButBottom.setWidth(60);
        menuButBottom.setUnactive();
        allButtons.add(menuButBottom);


        menuButMid = new InvisButton(screen.getButtMidX(), screen.getButtMidY());
        menuButMid.setHeight(60);
        menuButMid.setWidth(60);
        menuButMid.setUnactive();
        allButtons.add(menuButMid);


        menuButTop = new InvisButton(screen.getButtTopX(), screen.getButtTopY());
        menuButTop.setHeight(60);
        menuButTop.setWidth(60);
        menuButTop.setUnactive();
        allButtons.add(menuButTop);

        winkelmandjeButt = new InvisButton(screen.getWinkelmandButX(), screen.getWinkelmandButY());
        winkelmandjeButt.setHeight(60);
        winkelmandjeButt.setWidth(60);
        winkelmandjeButt.setUnactive();
        allButtons.add(winkelmandjeButt);


    }

    //  set de juiste knoppen actief
    public void setActiveScreen(Integer screen) {
        switch (screen) {

            // 0 = beginscherm
            // 1= hoofdmenu
            // 2= burger menu
            // 3= dranken menu
            // 4= friet menu
            // 5= winkelmandje
            case 0:
                bottomButt.setActive();


                break;
            case 1:
                backButt.setActive();
                drankenLink.setActive();
                frietLink.setActive();
                burgerLink.setActive();
                winkelmandjeButt.setActive();
                break;
            case 2:
            case 3:
            case 4:
                backButt.setActive();
                menuButTop.setActive();
                menuButMid.setActive();
                menuButBottom.setActive();
                bottomButt.setActive();
                break;
            case 5:
                //winkelmandje buttons
                backButt.setActive();
                bottomButt.setActive();
                break;

        }

    }

    //doe de actie als op actieve knop geklikt
    public Integer handleClicked(int screen) {


        switch (screen) {

            case 0:
                bottomButt.setClicked(mouseX, mouseY);
                if (bottomButt.buttonClicked()) {
                    screen = 1;
                    resetMouse();
                }


                break;
            case 1:

                backButt.setClicked(mouseX, mouseY);
                if (backButt.buttonClicked()) {
                    screen = 0;
                    resetMouse();
                }
                burgerLink.setClicked(mouseX, mouseY);
                // System.out.println(burgerLink.getState());
                if (burgerLink.buttonClicked()) {
                    screen = 2;
                    resetMouse();
                }
                drankenLink.setClicked(mouseX, mouseY);
                if (drankenLink.buttonClicked()) {
                    screen = 3;
                    resetMouse();
                }
                frietLink.setClicked(mouseX, mouseY);
                if (frietLink.buttonClicked()) {
                    screen = 4;
                    resetMouse();
                }
                winkelmandjeButt.setClicked(mouseX, mouseY);
                if (winkelmandjeButt.buttonClicked()) {
                    screen = 5;
                    resetMouse();
                }

                break;
            case 2:
                // state 1 vega
                // state 2 vis
                //state 3 vlees
                backButt.setClicked(mouseX, mouseY);
                if (backButt.buttonClicked()) {
                    screen = 1;
                    resetMouse();
                }
                menuButTop.setClicked(mouseX, mouseY);
                if (menuButTop.buttonClicked()) {
                    //winkelmand functie +  id 1
                    winkelmandje.addItem(1);
                    resetMouse();
                }

                menuButMid.setClicked(mouseX, mouseY);
                if (menuButMid.buttonClicked()) {
                    winkelmandje.addItem(2);
                    //winkel mand functie
                    resetMouse();
                }
                menuButBottom.setClicked(mouseX, mouseY);
                if (menuButBottom.buttonClicked()) {
                    winkelmandje.addItem(3);
                    //winkelmand functie
                    resetMouse();
                }
                bottomButt.setClicked(mouseX, mouseY);
                if (bottomButt.buttonClicked()) {
                    screen = 1;
                    resetMouse();
                }

                break;
            case 3:
                // 4 Bier
                // 5 Cola
                // 6 7-UP
                backButt.setClicked(mouseX, mouseY);
                if (backButt.buttonClicked()) {
                    screen = 1;
                    resetMouse();
                }
                menuButTop.setClicked(mouseX, mouseY);
                if (menuButTop.buttonClicked()) {
                    //winkelmand functie +  id 1
                    winkelmandje.addItem(4);
                    resetMouse();
                }
                menuButMid.setClicked(mouseX, mouseY);
                if (menuButMid.buttonClicked()) {
                    winkelmandje.addItem(5);
                    //winkel mand functie
                    resetMouse();
                }
                menuButBottom.setClicked(mouseX, mouseY);
                if (menuButBottom.buttonClicked()) {
                    winkelmandje.addItem(6);
                    //winkelmand functie
                    resetMouse();
                }
                bottomButt.setClicked(mouseX, mouseY);
                if (bottomButt.buttonClicked()) {
                    screen = 1;
                    resetMouse();
                }
                break;
            case 4:
                // 7 friet ras
                // 8 friet krul
                // 9 friet normaal
                backButt.setClicked(mouseX, mouseY);
                if (backButt.buttonClicked()) {
                    screen = 1;
                    resetMouse();
                }
                menuButTop.setClicked(mouseX, mouseY);
                if (menuButTop.buttonClicked()) {
                    //winkelmand functie +  id 1
                    winkelmandje.addItem(7);
                    resetMouse();
                }
                menuButMid.setClicked(mouseX, mouseY);
                if (menuButMid.buttonClicked()) {
                    winkelmandje.addItem(8);
                    //winkel mand functie
                    resetMouse();
                }
                menuButBottom.setClicked(mouseX, mouseY);
                if (menuButBottom.buttonClicked()) {
                    winkelmandje.addItem(9);
                    //winkelmand functie
                    resetMouse();
                }
                bottomButt.setClicked(mouseX, mouseY);
                if (bottomButt.buttonClicked()) {
                    screen = 1;
                    resetMouse();
                }
                break;
            case 5:
                //winkelmandje buttons
                backButt.setClicked(mouseX, mouseY);
                if (backButt.buttonClicked()) {
                    screen = 1;
                    resetMouse();
                }
                bottomButt.setClicked(mouseX, mouseY);
                if (bottomButt.buttonClicked()) {
                    winkelmandje.emptyWinkelmandje();
                    screen = 0;
                    resetMouse();
                }

                break;

        }
        return screen;
    }

    public void setAllButtonsUnactive() {
        int x = allButtons.size();
        for (int i = 0; i < x; i++) {
            if (allButtons.get(i).getState()) {
                allButtons.get(i).setUnactive();
            }
        }

    }

    private void setActiveButtons() {
        int x = allButtons.size();
        for (int i = 0; i < x; i++) {
            if (allButtons.get(i).getState()) {
                activeButtons.add(allButtons.get(i));
            }
        }
    }

    public int getMouseX() {
        return mouseX;
    }

    public void setMouseX(int mouseX) {
        this.mouseX = mouseX;
    }

    public int getMouseY() {
        return mouseY;
    }

    public void setMouseY(int mouseY) {
        this.mouseY = mouseY;
    }

    public void resetMouse() {
        mouseX = -1;
        mouseY = -1;
    }
    public Winkelmandje winkelmandje(){
        return winkelmandje;
    }

}

