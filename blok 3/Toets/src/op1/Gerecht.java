package op1;

public class Gerecht extends MenuItem {

//    FIXME Private maken
    public String langeOmschrijving;


    public Gerecht(String omschrijving, double prijs, String langeOmchsrijving) {
        super(omschrijving, prijs);
        this.langeOmschrijving = langeOmchsrijving;
    }

    public String getLangeOmschrijving() {
        return langeOmschrijving;
    }

    public void setLangeOmschrijving(String langeOmschrijving) {
        this.langeOmschrijving = langeOmschrijving;
    }

    public String showGerecht() {
        String output = "gerecht :" + getKorteOmschrijving() +
                "\nprijs :" + getPrijs() +
                "\nomschrijving: " + getLangeOmschrijving();
        return output;
    }

    //TODO ik mis een toString
}
