package op1;

public class Dranken extends MenuItem {
//    FIXME private maken
    public double percentage;
    public int inhoudCL;


    public Dranken(String korteOmschrijving, double prijs, double percentage, int inhoudCL) {
        super(korteOmschrijving, prijs);
        this.percentage = percentage;
        this.inhoudCL = inhoudCL;
    }

    public double getPercentage() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }

    public int getInhoudCL() {
        return inhoudCL;
    }

    public void setInhoudCL(int inhoudCL) {
        this.inhoudCL = inhoudCL;
    }

    public String showDrank() {
        String output = "Drank: " + getKorteOmschrijving() +
                "\nprijs: " + getPrijs() +
                "\npercentage: " + getPercentage() +
                "\ninhoud in CL : " + inhoudCL;
        return output;
    }

    //TODO ik mis een toString

}
