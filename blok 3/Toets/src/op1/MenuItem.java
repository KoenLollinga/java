package op1;

public class MenuItem {
//   FIXME Private maken
   public String omschrijving;
   public double prijs;
   private int items=0;
   private int id;

    public MenuItem(String korteOmschrijving,double prijs) {
        this.omschrijving = korteOmschrijving;
        this.prijs = prijs;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void addItem(){
        if (items < 49){
            items++;
        }
    }
    public int getItems(){
        return items;
    }


    public void setKorteOmschrijving(String omschrijving) {
        this.omschrijving = omschrijving;
    }

    public void setPrijs(double prijs) {
        this.prijs = prijs;
    }

    public String getKorteOmschrijving() {
        return omschrijving;
    }

    public double getPrijs() {
        return prijs;
    }

    public String showMenuItem() {
        String output = "item :" + getKorteOmschrijving() +
                "\nprijs :" + getPrijs() ;
        return output;
    }

    //TODO ik mis een toString


}
