package op1;

public class Main {
    public static void main(String[] args) {
        String korteOmschrijving = "Hamburger", drankOmschr = "Aardbei milkshake";
        String langeOmschrijving = "Vers gebakken hamburger met de beste gebakken uitjes";
        double prijsGerecht = 1.25, prijsDrank = 3.45;
        double percentage = 0.9;
        int inhoud = 50;
        Gerecht gerecht;
        Dranken drank;


        gerecht = new Gerecht(korteOmschrijving, prijsGerecht, langeOmschrijving);
        drank = new Dranken(drankOmschr, prijsDrank, percentage, inhoud);

        System.out.println(drank.showDrank() + "\n\n" + gerecht.showGerecht());

//        TODO Nog meer test.

    }
}
