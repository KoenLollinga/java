import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

import java.util.ArrayList;

public class RekButtons {
    private ArrayList<Button> butNumbers, butOperators;
    private ArrayList<String> stOperators;
    private RekCalculate rekCalculate;
    private Text text;


    public int numBaseX = 0, numBaseY = 150, butWidth = 45, butHeight = 55, butRowAmount = 3;
    private int resetX = numBaseX, resetY = numBaseY;
    public int opBaseX = resetX+(butWidth*butRowAmount), opBaseY =(numBaseY+butHeight)+11;
    public int butOpHeight =(butHeight*4)/5;


    public RekButtons(Pane pane) {

        butNumbers = new ArrayList<Button>();


        for (int i = 0; i < 10; i++) {

            butNumbers.add(new Button(Integer.toString(i)));
            butNumbers.get(i).setMinWidth(butWidth);
            butNumbers.get(i).setMinHeight(butHeight);

            if (i == 0) {
                butNumbers.get(i).setLayoutX(numBaseX + butWidth);
                butNumbers.get(i).setLayoutY(numBaseY + butHeight);

            } else if (i == 1) {
                butNumbers.get(i).setLayoutX(numBaseX);
                butNumbers.get(i).setLayoutY(numBaseY);

            } else {
                butNumbers.get(i).setLayoutX(numBaseX += butWidth);

                if (i % butRowAmount == 1) {
                    numBaseY -= butHeight;
                    numBaseX = resetX;
                }

                butNumbers.get(i).setLayoutY(numBaseY);
                butNumbers.get(i).setLayoutX(numBaseX);

            }

        }
        numBaseX = resetX;
        numBaseY= resetY;

        //knopen op de juiste plek
        butNumbers.add(10,new Button("del"));
        butNumbers.get(10).setMinWidth(butWidth);
        butNumbers.get(10).setMinHeight(butHeight);
        butNumbers.get(10).setLayoutY(numBaseY + butHeight);
        butNumbers.get(10).setLayoutX(numBaseX+(butWidth*2));

        butNumbers.add(11,new Button("."));
        butNumbers.get(11).setMinWidth(butWidth);
        butNumbers.get(11).setMinHeight(butHeight);
        butNumbers.get(11).setLayoutY(numBaseY + butHeight);
        butNumbers.get(11).setLayoutX(numBaseX);

        stOperators = new ArrayList<>();
        stOperators.add("=");
        stOperators.add("+");
        stOperators.add("-");
        stOperators.add("*");
        stOperators.add("/");
        // buton text

        text = new Text();
        text.setText("0");
        text.setLayoutX(numBaseX);
        text.setLayoutY(numBaseY-(butHeight*2)-10);
        text.maxWidth(butWidth*(butRowAmount+1));

        int x = stOperators.size();
        butOperators = new ArrayList<Button>();

        for (int i = 0; i < x; i++) {
            butOperators.add(new Button(stOperators.get(i)));
            butOperators.get(i).setMinHeight(butOpHeight);
            butOperators.get(i).setMinWidth(butWidth);

            butOperators.get(i).setLayoutY(opBaseY-(butOpHeight*i));
            butOperators.get(i).setLayoutX(opBaseX);

        }

        rekCalculate = new RekCalculate(butNumbers,butOperators,text);



    }


    public void showButtons(Pane pane) {
        pane.getChildren().addAll(butNumbers);
        pane.getChildren().addAll(butOperators);
        pane.getChildren().add(text);

    }

    public void removeButtons(Pane pane) {
        pane.getChildren().removeAll(butNumbers);
        pane.getChildren().removeAll(butOperators);
        pane.getChildren().add(text);

    }

}


