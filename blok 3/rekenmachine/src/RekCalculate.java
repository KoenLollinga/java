import javafx.scene.control.Button;
import javafx.scene.text.Text;

import java.util.ArrayList;

public class RekCalculate {

    private ArrayList<Button> butNumbers, butOperators;
    private String $number = "0";
    private int operator;
    private float flNumber1, flNumber2, flAwnser;
    private String $anwser;

    public RekCalculate(ArrayList<Button> butNumbers, ArrayList<Button> butOperators, Text display) {
        this.butNumbers = butNumbers;
        this.butOperators = butOperators;


        for (int i = 0; i < butOperators.size(); i++) {
            int finalI = i; //FIXME is dit nodig?

            butOperators.get(i).setOnAction(ev -> {
                if ($number != "0") {
                    display.setText(handleButOperators(finalI)); //FIXME kan gewoon i zijn?
                }
            });


        }

        for (int i = 0; i < butNumbers.size(); i++) {
            int finalI = i; //FIXME is dit nodig?
            butNumbers.get(i).setOnAction(ev -> {
                ; //FIxme, volgens mij kan dit weg
                display.setText(handleButNumbers(finalI)); //FIXME kan gewoon i zijn?
            });
        }


    }

    public String handleButNumbers(Integer butNumbers) {

        String newNumber = butNumbers.toString();

        switch (butNumbers) {
            case 10:
                if ($number.length() > 1) {
                    $number = $number.substring(0, $number.length() - 1);
                } else {
                    $number = "0";
                }
                break;
            case 11:
                if (!$number.contains(".")) {
                    $number += ".";
                }
                break;
        }


        if (Integer.parseInt(newNumber) < 10) { // FIXME butNumber < 10?
            if ($number == "0") {
                $number = newNumber;
            } else {
                $number += newNumber;
            }
        }

        System.out.println("current number = " + $number);
        return $number;
    }


    public String handleButOperators(Integer index) {
//     0   stOperators.add("=");
//     1   stOperators.add("+");
//     2   stOperators.add("-");
//     3   stOperators.add("*");
//     4   stOperators.add("/");

        flNumber1 = Float.parseFloat($number);
        $number = "0";
        System.out.println( flNumber1 == 0);


        switch (index) {

            case 0:
                //=

                //FIXME wat als flNumber 1 en 2 bijde 0 zijn wat is flAnswer?
                if (flNumber1 != 0 && flNumber2 == 0) {
                    flNumber2 = flNumber1;
                } else if (flNumber1 != 0 && flNumber2 != 0) {
                    switch (operator) {
                        case 1:
                            flNumber2 = flNumber1 + flNumber2;
                            flAwnser = flNumber2;
                            break;
                        case 2:
                            flNumber2 = flNumber1 - flNumber2;
                            flAwnser = flNumber2;
                            break;
                        case 3:
                            flNumber2 = flNumber1 * flNumber2;
                            flAwnser = flNumber2;
                            break;
                        case 4:
                            flNumber2 = flNumber1 / flNumber2;
                            flAwnser = flNumber2;
                            break;
                    }
                    flNumber1 = 0;
                    flNumber2 = 0;

                }
                //end case 0
                break;
            case 1:
                //+
                if (flNumber1 != 0 && flNumber2 == 0) {
                    flNumber2 = flNumber1;
                } else if (flNumber1 != 0 && flNumber2 != 0) {
                    flNumber2 = flNumber1 + flNumber2;
                }
                operator = 1;
                flAwnser = flNumber2;

                break;
            case 2:
                //-
                if (flNumber1 != 0 && flNumber2 == 0) {
                    flNumber2 = flNumber1;
                } else if (flNumber1 != 0 && flNumber2 != 0) {
                    flNumber2 = flNumber1 - flNumber2;
                }
                operator = 2;
                flAwnser = flNumber2;

                break;
            case 3:
                //*
                if (flNumber1 != 0 && flNumber2 == 0) {
                    flNumber2 = flNumber1; //FIXME klopt niet! Zo'n bank wil ik wel hebben!!

                } else if (flNumber1 != 0 && flNumber2 != 0) {
                    flNumber2 = flNumber1 * flNumber2;
                }
                operator = 3;
                flAwnser = flNumber2;
                break;
            case 4:
                //  /
                if (flNumber1 != 0 && flNumber2 == 0) {
                    flNumber2 = flNumber1; //FIXME klopt niet!

                } else if (flNumber1 != 0 && flNumber2 != 0) {
                    flNumber2 = flNumber1 / flNumber2;
                }
                operator = 4;
                flAwnser = flNumber2;
                break;


        }
        System.out.println("number 1 " + flNumber1);
        System.out.println("number 2 " + flNumber2);

        $anwser = Float.toString(flAwnser);


        return $anwser;
    }




}



